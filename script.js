const textbox = document.getElementById("textInput");
const lettersDiv = document.getElementById("lettersDiv");
const wordsDiv = document.getElementById("wordsDiv")


function getLetterCounts (typedText) {
   const letterCounts = {};
   for (let i = 0; i < typedText.length; i++) {
      currentLetter = typedText[i];
      if (letterCounts[currentLetter] === undefined) {
         letterCounts[currentLetter] = 1;
      } else {
         letterCounts[currentLetter] += 1;
      }
   }
   return letterCounts;
}
function printLetterCounts (letterCounts) {
   for (let letter in letterCounts) {
       const span = document.createElement("span");
      const textContent = document.createTextNode('"' + letter + "\": " + letterCounts[letter] + ", ");
      span.appendChild(textContent);
      lettersDiv.appendChild(span);
   }
}
function getWordCounts(typedText) {
const words = typedText.split(/\s/);
wordCounts= {};
for (let k = 0; k < words.length; k++) {
   currentWord = words[k];
   if (wordCounts[currentWord] === undefined) {
      wordCounts[currentWord] = 1;
   } else {
      wordCounts[currentWord] += 1;
   }
}
return wordCounts;
}

function printWordCounts (wordCounts) {
   for (let word in wordCounts) {
       const span = document.createElement("span");
      const textContent = document.createTextNode('"' + word + "\": " + wordCounts[word] + ", ");
      span.appendChild(textContent);
      lettersDiv.appendChild(span);
   }
}
document.getElementById("countButton").onclick = function () {
   const typedText = textbox.value
      .toLowerCase()
      .replace(/[^a-z'\s]+/g, "");

      printLetterCounts(getLetterCounts(typedText));

   printWordCounts(getWordCounts(typedText));
}


